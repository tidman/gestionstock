import { Injectable } from '@angular/core';
import { Produit, stock } from './produit';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  stock : Array<Produit>;

  constructor() {
    this.stock = stock;
  }

  add(produit : Produit){
    this.stock.push(produit);
  }

  remove (index:number){
    this.stock.splice(index, 1);
  }

  list (){
    return this.stock;
  } 
}
