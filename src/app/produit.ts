
export class Produit {
    nom: string;
    fournisseur : string;
    emailFournisseur : string;
    ingredients : Array<string>;
    description : string;
    age : number;
    conditionConservation : string;
    prix : number;

    constructor(nom: string, fournisseur : string, emailFournisseur : string, ingredients : Array<string>, description : string, age : number, conditionConservation : string, prix : number){
        this.nom = nom;
        this.fournisseur = fournisseur;
        this.emailFournisseur = emailFournisseur;
        this.ingredients = ingredients;
        this.description = description;
        this.age = age;
        this.conditionConservation = conditionConservation;
        this.prix = prix;
    }
}

export const stock : Array<Produit> = [
                new Produit("Produit 1", "Fournisseur 1", "fournsiseur@mail.com", ['ingredient 1', 'ingredient 2'], "Description", 5, "condition de conservation", 23.23),
                new Produit("Produit 2", "Fournisseur 2", "fournsiseur2@mail.com", ['ingredient 1', 'ingredient 2'], "Description", 4, "condition de conservation", 1.00), 
                new Produit("Produit 3", "Fournisseur 1", "fournsiseur@mail.com", ['ingredient 1', 'ingredient 2'], "Description", 5, "condition de conservation", 78.99),
                new Produit("Produit 4", "Fournisseur 2", "fournsiseur2@mail.com", ['ingredient 4', 'ingredient 6'], "Description", 1, "condition de conservation", 0.67),
                new Produit("Produit 5", "Fournisseur 3", "fournsiseur3@mail.com", ['ingredient 1', 'ingredient 2'], "Description", 65, "condition de conservation", 194.23)];
