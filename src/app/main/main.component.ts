import { Component, OnInit } from '@angular/core';
import { StockService } from '../stock.service';
import { FormBuilder, FormGroup, FormControl, Validators } from'@angular/forms';
import { Produit } from '../produit';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  stock : Array<Produit>;
  isHidden : boolean = true;
  nomCtrl : FormControl;
  fournisseurCtrl : FormControl;
  ageCtrl : FormControl;
  descriptionCtrl : FormControl;
  produitForm: FormGroup;

  constructor(private stockService : StockService , fb:FormBuilder) { 
    this.nomCtrl = fb.control('',Validators.required);
    this.fournisseurCtrl = fb.control('',Validators.required); 
    this.ageCtrl = fb.control('',[Validators.required, Validators.pattern("^[0-9]*$")]); 
    this.descriptionCtrl = fb.control('',Validators.required); 
    this.produitForm = fb.group({
      nom: this.nomCtrl,
      fournisseur: this.fournisseurCtrl,
      age: this.ageCtrl,
      description: this.descriptionCtrl,
    })
  }

  ngOnInit() {
    this.stock = this.stockService.list();
  } 

  delete(produit:Produit){
    var index = this.stock.indexOf(produit);
    this.stockService.remove(index);
    this.stock = this.stockService.list();
  }

  add(){
    this.isHidden = false;
  }

  validate(){
    this.stockService.add(this.produitForm.value);
    this.isHidden = true;    
    this.nomCtrl.reset();
    this.fournisseurCtrl.reset();
    this.ageCtrl.reset();
    this.descriptionCtrl.reset();
  }

}
